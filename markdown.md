Final WE01 Marion Durand
=======

Introduction
----
Aujourd'hui, nous utilisons le web dans notre **vie quotidienne**, généralement sans nous poser de questions. Cependant, il est loin d'être exempt de défaut et il peut être intéressant de s'intéresser à des alternatives afin de mieux percevoir ces défauts et de pouvoir améliorer le service rendu, que ça soit par une évolution du web ou bien par l'utilisation de services alternatifs qui sera plus adaptée.


Problèmes liés à la complexification du web
----

Le web est un réseau qui se repose sur une série de protocoles qui sont de plus complexes et qui sont nombreux
Comme il est souligné dans l'article, la **complexité du web pose des problèmes** non seulement aux développeurs mais aussi **pour tous les utilisateurs**. En effet, la complexification du web entraine l'arrivée de logiciels beaucoup plus complexes. Cela pose des problèmes dans de nombreux domaines. 

Tout d'abord, cela **réduit la concurrence** (et donc la liberté de choix du consommateur) puisque développer un logiciel pour le web est devenu extrêmement complexe, ainsi peu de compagnies en ont les capacités et encore moins vont réellement se lancer dans cette aventure qui est une tâche colossale.

L'impact est aussi **environnemental**. En effet, ces technologies utilisent beaucoup d'opérations (parfois réaliser sur des machines distantes) ainsi, cette abondance d'opérations effectuées augmente la consommation de ressources.

L'impact est aussi sensible sur la **rapidité du web**. En effet les différentes opérations effectuées prennent du temps et mis bout à bout, l'augmentation peut être sensible. De même les sites étant eux-mêmes de plus en plus complexe, il faut de plus en plus de temps pour transférer les données nécessaires à leur affichage.

De plus l'augmentation des calculs du côté des serveurs **met parfois en péril leur capacité à continuer leurs services en cas d'affluence importante**.

Finalement une partie de la complexité du web vient de contenus tiers qui agacent les utilisateurs des sites. On peut cité l'exemple de la publicité qui génère des opérations afin de déterminer les publicités les plus efficaces à envoyer à un utilisateur ou encore celui des cookies qui permettent de traquer les utilisateurs sans qu'ils en ait vraiment conscience ni envies.

Des solutions plus ou moins radicales
----

Ainsi nous avons évoqué dans la partie précédente une partie des problèmes liée à la complexification du web. On peut donc se poser la question de ce que l'on peut faire pour régler ce problème.

Dans l'article complet l'auteur de l'article présente comme solution partielle l'utilisation de **navigateur web léger** (comme "Dillo" ou "Tor Browser"). Cependant ces solutions ne sont que **très partielles**. En effet même si cela limite un peu la complexité du web en limitant l'utilisation de certaines technologies (comme le CSS, le JavaScripte) ou en supprimant les requêtes publicitaires, ces navigateurs se heurtent à un problème majeur : les sites web qu'ils essayent d'afficher ne sont pas prévus pour fonctionner sans ces technologies. Ainsi **certains sites peuvent mal fonctionner** ou ne pas fonctionner du tout. Certains créateurs de site se reposent sur des technologies normalement accessoires pour créer le cœur de leur site ce qui pose des problèmes lorsque l'on veut les retirer. Ainsi pour que cette solution soit réellement viable, cela implique une modification radicale des pratiques des créateurs de site pour les rendre consultables normalement même sans les technologies accessoires et au besoin intégrer en bonus des technologies accessoires pour proposer à ceux qui le souhaitent un affichage un peu plus complexe. 

La deuxième solution proposée par l'auteur de l'article est une solution radicale. En effet il s'agit d'un tout **nouveau réseau**, distinct du web (même s’il s'en inspire), qui est conçu pour être beaucoup **plus simple et plus difficile à faire évoluer**. En effet une trop grande possibilité d'évolution risque d'entrainer une grande complexification du nouveau réseau. Ainsi en limitant les possibilités d'évolution cela incitera les futurs utilisateurs et mainteneurs du réseau à rester dans la simplicité.
Il s'agit ici de ne pas reproduire l'exemple du web qui a commencé assez simple et qui est désormais beaucoup plus complexe.

L'utilisation de **chaînes éditoriales** peuvent aider à la mise en place de ce réseau, en effet en se contentant de **géré le contenu et sa structure** et en offrant la possibilité de gérer la **mise en page à postériori** (et de la personnaliser), cela permettrait d'avoir le contenu facilement accessible avec la technologie principale tout en permettant à un utilisateur désireux d'avoir une mise en page un peu plus excentrique d'afficher les pages en suivant ces envies.
Ainsi, si le créateur d'une page ne se préoccupe plus de la mise en page mais simplement du contenu. Ainsi l'affichage que le créateur utilise n'influx pas sur l'affichage final. Donc même si le créateur a un affichage complexe A, un visiteur pourra sans aucun problème consulter le site avec un affichage simplifié B puisque la chaîne éditoriale se charge de la mise en page.
Ainsi même en version simplifiée un site restera consultable facilement et lisible par tous, peu importe la version de l'affichage qu'ils possèdent.
**La mise en page devient indépendante du contenu qui est simplement structuré. **


Conclusion
----
Ainsi nous avons vu en quoi la complexité du web pouvait être problématique pour tous les utilisateurs et non pas seulement les développeurs.
Nous avons aussi vu que des solutions existaient. On peut par exemple utiliser un navigateur léger qui ne chargera que le contenu utilisant les technologies essentielles du web. Cependant cette solution n'est que partielle car beaucoup de sites web ne sont pas conçus pour fonctionner sans les technologies auxiliaires et ainsi ces navigateurs légers ne fonctionnent pas toujours et il est difficile de s'en rendre compte car aucun message d'erreur ni d'explication n'est fourni.
Une solution plus radicale consiste à créer un nouveau réseau distinct en ne partant de rien (ou en s'inspirant seulement du web). Cette solution pourrait être complétée avec l'utilisation des chaînes éditoriales qui permettent de géré de manière séparer le contenu et la présentation de celui-ci.


Sources: 
- [sujet](https://librecours.net/module/we01/2020H-final/2020H-final.xhtml)
- [article présenté dans le sujet](https://framablog.org/2020/12/30/le-web-est-il-devenu-trop-complique/)
- [cours sur le fonctionnement d'internet](https://librecours.net/module/sr/fonctionnement-internet/)
- [cours sur les chaînes éditoriales](https://librecours.net/module/doc/ced01/)

License: [CC-BY CA](https://creativecommons.org/licenses/by-sa/2.0/fr/)

*Cette licence permet de partager et de modifier le document. Il est par contre obligatoire de créditer le document original en cas de réutilisation et de partager avec la même licence.*
